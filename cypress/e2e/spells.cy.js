/// <reference types="Cypress" />

describe("template spec", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8080/");
    cy.get('[href="/spells"]').click();
  });

  it("should be in the correct url", () => {
    cy.url().should("include", "/spells");
  });

  it("should add mode by defect", () => {
    cy.get("#cancelButtonTest").should("be.visible");
    cy.get("#addButtonTest").should("be.visible");
    cy.get("#updateButtonTest").should("not.exist");
    cy.get("#deleteButtonTest").should("not.exist");
  });

  it("should active the add mode with the cancel button", () => {
    cy.get("#spellTest").click();
    cy.get("#cancelButtonTest").click();
    cy.get("#cancelButtonTest").should("be.visible");
    cy.get("#addButtonTest").should("be.visible");
    cy.get("#updateButtonTest").should("not.exist");
    cy.get("#deleteButtonTest").should("not.exist");
  });

  it("should show clicked spell in the form", () => {
    cy.get(":nth-child(5) > #spellTest > #nameUITest").click();
    cy.get(":nth-child(5) > #spellTest > #descriptionUITest").click();
    cy.get("#inputNameTest").should("have.value", "Scourgify");
    cy.get("#inputDescriptionTest").should("have.value", "Cleans things");
  });

  it("should be to show the update/remove mode when clicking on a spell", () => {
    cy.get(":nth-child(1) > #spellTest > #nameUITest").click();
    cy.get(":nth-child(1) > #spellTest > #descriptionUITest").click();
    cy.get("#cancelButtonTest").should("be.visible");
    cy.get("#updateButtonTest").should("be.visible");
    cy.get("#deleteButtonTest").should("be.visible");
    cy.get("#addButtonTest").should("not.exist");
  });

  it("should add a new spell to the list", () => {
    cy.get("#inputNameTest").type("Revelio");
    cy.get("#inputDescriptionTest").type("Reveal objects and hidden messages");
    cy.get("#addButtonTest").click();
    cy.get("#spellTest:last-child")
      .should("contain", "Revelio")
      .should("contain", "Reveal objects and hidden messages");
  });

  it("should update a existing spell of the list", () => {
    cy.get(":nth-child(4) > #spellTest > #nameUITest").click();
    cy.get("#inputNameTest").clear().type("Wingardium Leviosa");
    cy.get("#updateButtonTest").click();
    cy.get(":nth-child(4) > #spellTest > #nameUITest").should(
      "have.text",
      "Wingardium Leviosa"
    );
  });

  it("should remove a existing spell of the list", () => {
    cy.get(":nth-child(11) > #spellTest > #nameUITest").click();
    cy.get("#deleteButtonTest").click();
    cy.get("#listTest").should("not.have.text", "Pack");
  });
});
