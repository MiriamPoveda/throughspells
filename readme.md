<div style="text-align:center">

# Through Spells

Welcome to the official website of the British Ministry of Magic!

This page will only be visible to witches and wizards. If you are in the presence of a muggle, do not speak publicly about its content!

No further... Go on!

[Through Spells](https://through-spells.web.app/)

___

Sincerely

Cornelius Fudge (Minister for Magic)

<img src="./src/images/ministry.png" alt="Logo of the Ministry of Magic" width="250px">
</div>
