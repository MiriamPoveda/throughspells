import { SpellsRepository } from "../src/repositories/spells.repository";
import { AllSpellsUseCase } from "../src/usecases/all-spells.usecase";
import { SPELLS } from "./fixtures/spells";

jest.mock("../src/repositories/spells.repository");

describe("Use case of all spells", () => {
  beforeEach(() => {
    SpellsRepository.mockClear();
  });

  it("Should get all spells", async () => {
    SpellsRepository.mockImplementation(() => {
      return {
        getAllSpells: () => {
          return SPELLS;
        },
      };
    });

    const spells = await AllSpellsUseCase.execute();

    expect(spells.length).toBe(72);
    expect(spells[0].name).toBe(SPELLS[0].spell);
    expect(spells[0].description).toBe(SPELLS[0].use);
  });
});
