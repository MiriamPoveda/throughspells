import { SpellsRepository } from "../src/repositories/spells.repository";
import { AddSpellUseCase } from "../src/usecases/add-spell.usecase";

jest.mock("../src/repositories/spells.repository");

describe("Use case of add spell", () => {
  beforeEach(() => {
    SpellsRepository.mockClear();
  });
  it("Should add a new spell", async () => {
    const SPELLS = [
      {
        id: 1,
        name: "Accio",
        description: "Summoning charm",
      },
      {
        id: 2,
        name: "Glisseo",
        description: "Turns a staircase into a slide",
      },
    ];

    const spell = {
      name: "Revelio",
      description:
        "Reveals hidden objects, messages, invisible things and secret passages",
    };

    SpellsRepository.mockImplementation(() => {
      return {
        addSpell: () => {
          return {
            id: 73,
            spell: spell.name,
            use: spell.description,
          };
        },
      };
    });

    const listUpdated = await AddSpellUseCase.execute(SPELLS, spell);

    expect(listUpdated.length).toBe(SPELLS.length + 1);
    expect(listUpdated[0].name).toBe(spell.name);
    expect(listUpdated[0].description).toBe(spell.description);
  });
});
