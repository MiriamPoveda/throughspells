import { Spell } from "../src/model/spell";
import { SpellsRepository } from "../src/repositories/spells.repository";
import { UpdateSpellUseCase } from "../src/usecases/update-spell.usecase";

jest.mock("../src/repositories/spells.repository");

describe("Use case of update spell", () => {
  beforeEach(() => {
    SpellsRepository.mockClear();
  });

  it("Should update a spell", async () => {
    const spell = new Spell({
      id: 8,
      name: "Lumos maxima",
      description: "Makes wand to emit a blinding flash of white light",
    });

    SpellsRepository.mockImplementation(() => {
      return {
        updateSpell: () => {
          return {
            id: spell.id,
            spell: spell.name,
            use: spell.description,
          };
        },
      };
    });

    const SPELLS = [
      {
        id: 8,
        name: "Lumos",
        description: "Makes wand emit light",
      },
      {
        id: 9,
        name: "Reparo",
        description: "Repairs broken objects",
      },
    ];

    const listUpdated = await UpdateSpellUseCase.execute(SPELLS, spell);

    expect(listUpdated.length).toBe(2);
    expect(listUpdated[0].name).toBe(spell.name);
    expect(listUpdated[0].description).toBe(spell.description);
  });
});
