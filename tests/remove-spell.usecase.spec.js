import { SpellsRepository } from "../src/repositories/spells.repository";
import { RemoveSpellUseCase } from "../src/usecases/remove-spell.usecase";

jest.mock("../src/repositories/spells.repository");

describe("Use case of remove spell", () => {
  beforeEach(() => {
    SpellsRepository.mockClear();
  });

  it("Should remove a spell", async () => {
    SpellsRepository.mockImplementation(() => {
      return {
        removeSpell: () => {},
      };
    });

    const SPELLS = [
      {
        id: 1,
        name: "Accio",
        description: "Summoning charm",
      },
      {
        id: 2,
        name: "Glisseo",
        description: "Turns a staircase into a slide",
      },
    ];

    const spellId = 1;

    const listUpdated = await RemoveSpellUseCase.execute(SPELLS, spellId);

    expect(listUpdated.length).toBe(SPELLS.length - 1);
    expect(listUpdated[0].id).toBe(2);
    expect(listUpdated[0].name).toBe("Glisseo");
  });
});
