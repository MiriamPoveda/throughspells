import { LitElement, html } from "lit";

export class ButtonUI extends LitElement {
  static get properties() {
    return {
      button: { type: String },
    };
  }

  render() {
    return html` <button id="buttonUI">${this.button}</button> `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("button-ui", ButtonUI);
