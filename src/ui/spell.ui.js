import { LitElement, html } from "lit";

export class SpellUI extends LitElement {
  static get properties() {
    return {
      spell: { type: Object },
    };
  }

  render() {
    return html`
      <h4 id="nameUITest">${this.spell?.name}</h4>
      <p id="descriptionUITest">${this.spell?.description}</p>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("spell-ui", SpellUI);
