import "../components/spells.component";
import "../components/detail.component";

export class SpellsPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
            <spells-detail id="details"></spells-detail>
            <spells-list class="listComponent" id="spells"></spells-list>
        `;

    const detailComponent = this.querySelector("#details");
    const spellsComponent = this.querySelector("#spells");

    this.addEventListener("select:several", (event) => {
      detailComponent.getFormValues(event.detail.selectSpell);
      detailComponent.enableAddMode(event.detail.enableAddMode);
    });

    this.addEventListener("modify:list", (event) => {
      detailComponent.enableAddMode(event.detail.enableAddMode);

      const action = event.detail;
      if (action.addSpell) {
        spellsComponent.addSpell(action.addSpell);
      }
      if (action.updateSpell) {
        spellsComponent.updateSpell(action.updateSpell);
      }
      if (action.removeSpell) {
        spellsComponent.removeSpell(action.removeSpell);
      }
    });
  }
}

customElements.define("spells-page", SpellsPage);
