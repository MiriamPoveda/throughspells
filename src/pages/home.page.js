export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <section class="homeSection">
    <h3 class="homeSection__homeTitle">Welcome</h3>
    <div>
    <p class="homeSection__homeParagraphs">Here you will be able to learn about a large number of spells that are currently in operation in our world.</p>
    <p class="homeSection__homeParagraphs">Feel free to update this list by removing unused spells, adding new ones or updating any of the existing ones.</p>
    </div>
    <h3 class="homeSection__homeTitle">Rules</h3>
    <div>
    <p class="homeSection__homeParagraphs">Providing false information about spells can lead to sanctions by the competent authority.</p>
    <p class="homeSection__homeParagraphs">The use of unforgivable curses (Imperius, Crucio and Avada Kedavra) and the invocation of the Dark Mark (Morsmordre) is absolutely forbidden.</p>
    <p class="homeSection__homeParagraphs">The use of any of these spells (or others) against Muggles is absolutely forbidden.</p>
    </div>
    </section>
    `;
  }
}

customElements.define("home-page", HomePage);
