import { Spell } from "../model/spell";
import { SpellsRepository } from "../repositories/spells.repository";

export class AllSpellsUseCase {
  static async execute() {
    const repository = new SpellsRepository();
    const spells = await repository.getAllSpells();
    return spells.map(
      (spell) =>
        new Spell({
          id: spell.id,
          name: spell.spell,
          description: spell.use,
        })
    );
  }
}
