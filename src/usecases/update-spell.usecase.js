import { SpellsRepository } from "../repositories/spells.repository";

export class UpdateSpellUseCase {
  static async execute(SPELLS = [], spellModel) {
    const repository = new SpellsRepository();
    const spellUpdated = await repository.updateSpell(spellModel);
    const spellUpdatedModel = {
      id: spellUpdated.id,
      name: spellUpdated.spell,
      description: spellUpdated.use,
    };

    return SPELLS.map((spell) =>
      spell.id === spellUpdatedModel.id ? spellUpdatedModel : spell
    );
  }
}
