import { SpellsRepository } from "../repositories/spells.repository";

export class RemoveSpellUseCase {
  static async execute(SPELLS = [], spellId) {
    const repository = new SpellsRepository();
    await repository.removeSpell(spellId);

    return SPELLS.filter((spell) => spell.id !== spellId);
  }
}
