import { Spell } from "../model/spell";
import { SpellsRepository } from "../repositories/spells.repository";

export class AddSpellUseCase {
  static async execute(SPELLS, spell) {
    const repository = new SpellsRepository();
    const newSpell = await repository.addSpell(spell);
    const newSpellModel = new Spell({
      id: newSpell.id,
      name: newSpell.spell,
      description: newSpell.use,
    });

    return [newSpellModel, ...SPELLS];
  }
}
