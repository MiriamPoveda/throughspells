import { LitElement, html } from "lit";
import { Spell } from "../model/spell";
import "../ui/button.ui";

export class DetailComponent extends LitElement {
  static get properties() {
    return {
      spellId: { type: Number },
      spellName: { type: String },
      spellDescription: { type: String },
      addMode: { type: Boolean },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.spellId = null;
    this.spellName = "";
    this.spellDescription = "";
    this.addMode = true;
  }

  render() {
    return html`
      <section class="detailSection">
        <form class="detailSection__detailForm">
          <div>
            <label htmlFor="spellName" class="detailLabels">Name</label>
            <input
              class="detailInputs"
              id="inputNameTest"
              type="text"
              name="spellName"
              .value="${this.spellName}"
              @change="${this.getUserNameValue}"
            />
          </div>
          <div>
            <label htmlFor="spellDescription" class="detailLabels"
              >Description</label
            >
            <input
              class="detailInputs"
              id="inputDescriptionTest"
              type="text"
              name="spellDescription"
              .value="${this.spellDescription}"
              @change="${this.getUserDescriptionValue}"
            />
          </div>
          ${this.addMode
            ? html` <div>
                <button-ui
                  id="cancelButtonTest"
                  button="Cancel"
                  @click="${this.handleResetForm}"
                ></button-ui>
                <button-ui
                  id="addButtonTest"
                  button="Add"
                  @click="${this.handleAddSpell}"
                ></button-ui>
              </div>`
            : html`<div>
                <button-ui
                  id="cancelButtonTest"
                  button="Cancel"
                  @click="${this.handleResetForm}"
                ></button-ui>
                <button-ui
                  id="updateButtonTest"
                  button="Update"
                  @click="${this.handleUpdateSpell}"
                ></button-ui>
                <button-ui
                  id="deleteButtonTest"
                  button="Delete"
                  @click="${this.handleRemoveSpell}"
                ></button-ui>
              </div>`}
        </form>
      </section>
    `;
  }

  getFormValues(spell) {
    this.spellId = spell.id;
    this.spellName = spell.name;
    this.spellDescription = spell.description;
  }

  enableAddMode(value) {
    this.addMode = value;
  }

  getUserNameValue(event) {
    this.spellName = event.target.value;
  }

  getUserDescriptionValue(event) {
    this.spellDescription = event.target.value;
  }

  cleanForm() {
    this.spellName = "";
    this.spellDescription = "";
  }

  handleResetForm(event) {
    event.preventDefault();
    this.addMode = true;
    this.cleanForm();
  }

  handleAddSpell(event) {
    event.preventDefault();
    const oneSpell = new Spell({
      name: this.spellName,
      description: this.spellDescription,
    });

    const spellToAdd = new CustomEvent("modify:list", {
      bubbles: true,
      composed: true,
      detail: {
        addSpell: oneSpell,
      },
    });
    this.dispatchEvent(spellToAdd);
    this.cleanForm();
  }

  handleUpdateSpell(event) {
    event.preventDefault();
    const oneSpell = new Spell({
      id: this.spellId,
      name: this.spellName,
      description: this.spellDescription,
    });

    const spellToUpdate = new CustomEvent("modify:list", {
      bubbles: true,
      composed: true,
      detail: {
        updateSpell: oneSpell,
      },
    });
    this.dispatchEvent(spellToUpdate);
    this.cleanForm();
  }

  handleRemoveSpell(event) {
    event.preventDefault();
    const oneSpell = this.spellId;

    const spellToRemove = new CustomEvent("modify:list", {
      bubbles: true,
      composed: true,
      detail: {
        removeSpell: oneSpell,
      },
    });
    this.dispatchEvent(spellToRemove);
    this.cleanForm();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("spells-detail", DetailComponent);
