import { LitElement, html } from "lit";
import { AllSpellsUseCase } from "../usecases/all-spells.usecase";
import { AddSpellUseCase } from "../usecases/add-spell.usecase";
import { UpdateSpellUseCase } from "../usecases/update-spell.usecase";
import { RemoveSpellUseCase } from "../usecases/remove-spell.usecase";
import "../ui/spell.ui";

export class SpellsComponent extends LitElement {
  static get properties() {
    return {
      spells: { type: Array },
      selectSpell: { type: Object },
      addMode: { type: Boolean },
      unforgivable: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.spells = await AllSpellsUseCase.execute();
    this.addMode = false;
    this.unforgivable = this.spells
      .filter((spell) =>
        ["Avada Kedavra", "Crucio", "Imperio", "Morsmordre"].includes(
          spell.name
        )
      )
      .map((spell) => spell.name.toLowerCase());
  }

  render() {
    return html`
      <section class="listSection">
        <ul class="listSection__fullList" id="listTest">
          ${this.spells?.map(
            (spell) => html`
              <li id="${spell.id}" @click="${this.handleSeveral}">
                <spell-ui
                  id="spellTest"
                  class="listSection--elementList ${this.unforgivable.includes(
                    spell.name.toLowerCase()
                  )
                    ? "unforgivableColor"
                    : ""}"
                  .spell="${spell}"
                ></spell-ui>
              </li>
            `
          )}
        </ul>
      </section>
    `;
  }

  handleSeveral(event) {
    const oneSpell = this.spells?.find(
      (spell) => spell.id === parseInt(event.currentTarget.id)
    );

    const severalOptions = new CustomEvent("select:several", {
      bubbles: true,
      composed: true,
      detail: {
        selectSpell: oneSpell,
        enableAddMode: false,
      },
    });
    this.dispatchEvent(severalOptions);
  }

  async addSpell(addSpell) {
    const existingSpell = this.spells.find(
      (spell) =>
        spell.name === addSpell.name ||
        spell.description === addSpell.description
    );
    if (existingSpell) {
      alert("This spell already exists in the list");
      return;
    }
    const addedSpell = await AddSpellUseCase.execute(this.spells, addSpell);
    this.spells = addedSpell;
  }

  async updateSpell(updateSpell) {
    const existingSpell = this.spells.find(
      (spell) =>
        spell.id !== updateSpell.id &&
        (spell.name === updateSpell.name ||
          spell.description === updateSpell.description)
    );
    if (existingSpell) {
      alert("This spell already exists in the list");
      return;
    }
    const updatedSpell = await UpdateSpellUseCase.execute(
      this.spells,
      updateSpell
    );
    this.spells = updatedSpell;
  }

  async removeSpell(removeSpell) {
    const removedSpell = await RemoveSpellUseCase.execute(
      this.spells,
      removeSpell
    );
    this.spells = removedSpell;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("spells-list", SpellsComponent);
