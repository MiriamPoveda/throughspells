import axios from "axios";

export class SpellsRepository {
  async getAllSpells() {
    return await (
      await axios.get("https://harry-potter-api-en.onrender.com/spells")
    ).data;
  }

  async addSpell(spell) {
    const dataSpell = {
      spell: spell.name,
      use: spell.description,
    };
    return await (
      await axios.post(
        "https://harry-potter-api-en.onrender.com/spells",
        dataSpell
      )
    ).data;
  }

  async updateSpell(spell) {
    const dataSpell = {
      id: spell.id,
      spell: spell.name,
      use: spell.description,
    };
    return await (
      await axios.put(
        `https://harry-potter-api-en.onrender.com/spells/${spell.id}`,
        dataSpell
      )
    ).data;
  }

  async removeSpell(spellId) {
    await axios.delete(
      `https://harry-potter-api-en.onrender.com/spells/${spellId}`
    );
  }
}
