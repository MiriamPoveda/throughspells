import "./main.css";
import { Router } from "@vaadin/router";

import "./pages/home.page";
import "./pages/spells.page";

const output = document.querySelector("#output");
const router = new Router(output);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/spells", component: "spells-page" },
  { path: "(.*)", redirect: "/" },
]);
